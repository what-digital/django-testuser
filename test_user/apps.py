from django.apps import AppConfig


class TestUserAppConfig(AppConfig):
    name = 'test_user'
